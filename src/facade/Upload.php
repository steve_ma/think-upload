<?php
namespace stevema\upload\facade;

use think\Facade;

/**
 * Class Captcha
 * @package think\captcha\facade
 * @mixin \stevema\afs\AfsManage
 */
class Upload extends Facade
{
    protected static function getFacadeClass()
    {
        return \stevema\upload\UploadManage::class;
    }
}