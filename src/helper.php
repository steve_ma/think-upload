<?php
use stevema\upload\UploadManage;
if (!function_exists('upload')) {
    function upload(): UploadManage
    {
        if(function_exists('app')) {
            return app('upload');
        }
        $upload = new UploadManage();
        $conf = ['default'=> 'local', 'local'=>[]];
        $upload->setConfig($conf);
        $upload->storage();
        return $upload;
    }
}